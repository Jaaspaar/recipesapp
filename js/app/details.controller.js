var recipesApp = angular.module('recipesApp', []);
angular.recipesApp = recipesApp;
recipesApp.controller('DetailsController', [
  '$scope',
  '$location',
  'recipesService',
  function (
    $scope,
    $location,
    recipesService) {
    $scope.recipe = getRequestedRecipe();

    function getRequestedRecipe() {
      const recipeId = parseInt(extractIdFromUrl($location.absUrl()));
      return recipesService.recipes.filter(r => r.id === recipeId)[0];
    }

    function extractIdFromUrl(url) {
      const idRegex = new RegExp('\\?id\\=([0-9]+)');
      const result = idRegex.exec(url);
      const ID_INDEX = 1;
      return result.length > ID_INDEX ? result[ID_INDEX] : 1;
    }
  }]);
