var recipesApp = angular.module('recipesApp', ['ui.bootstrap']);
angular.recipesApp = recipesApp;
recipesApp.controller('RecipesSearchController', [
  '$scope',
  '$filter',
  'recipesService',
  function (
    $scope,
    $filter,
    recipesService) {
    const NOT_APPLICABLE = 'na';
    const rawRecipes = recipesService.recipes;
    const rawIngredients = recipesService.ingredients;
    $scope.searchRecipe = {};
    $scope.searchRecipe.title = '';
    $scope.searchRecipe.minKcal = 0;
    $scope.searchRecipe.maxKcal = 1000;
    $scope.searchRecipe.mealType = NOT_APPLICABLE;
    $scope.searchRecipe.isVegetarian = NOT_APPLICABLE;
    $scope.recipes = rawRecipes;
    $scope.ingredients = rawIngredients;
    $scope.selectedIngredient;
    $scope.searchRecipe.selectedIngredients = [];

    $scope.$watch('searchRecipe', (searchRecipe) => {
      $scope.recipes = $filter('filter')(rawRecipes, (recipe) => {
        return recipe.title.toLowerCase().includes(searchRecipe.title.toLowerCase()) &&
          (!searchRecipe.minKcal || searchRecipe.minKcal < recipe.kcal)&&
          (!searchRecipe.maxKcal || searchRecipe.maxKcal > recipe.kcal) &&
          (searchRecipe.mealType === NOT_APPLICABLE || searchRecipe.mealType === recipe.mealType) &&
          (searchRecipe.isVegetarian === NOT_APPLICABLE || (searchRecipe.isVegetarian === 'true') === recipe.isVegetarian) &&
          isContainingSelectedIngredients(recipe);
      });
      $scope.statesWithFlags = [{'name':'Alabama','flag':'5/5c/Flag_of_Alabama.svg/45px-Flag_of_Alabama.svg.png'},{'name':'Alaska','flag':'e/e6/Flag_of_Alaska.svg/43px-Flag_of_Alaska.svg.png'}];
    }, true);

    $scope.onIngredientSelect = function(ingredient) {
      $scope.onIngredientDeselect(ingredient);
      $scope.searchRecipe.selectedIngredients.push(ingredient);
      $scope.selectedIngredient = '';
    };

    $scope.onIngredientDeselect = function(ingredient) {
      $scope.searchRecipe.selectedIngredients = $scope.searchRecipe.selectedIngredients.filter(el => {
        return el.name !== ingredient.name;
      })
    };

    function isContainingSelectedIngredients(recipe) {
      if($scope.searchRecipe.selectedIngredients.length === 0)
        return true;
      let isContaining = true;
      $scope.searchRecipe.selectedIngredients.forEach(selectedIng => {
        const requiredIngredient = recipe.ingredients.filter(recipeIng => {
          return recipeIng.name === selectedIng.name;
        });
        isContaining = requiredIngredient.length > 0 && isContaining;
      });
      return isContaining;
    }
  }]);
