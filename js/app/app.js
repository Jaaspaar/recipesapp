var recipesApp = angular.module('recipesApp', []);
angular.recipesApp = recipesApp;
recipesApp.controller('RecipesController', [
  '$scope',
  'recipesService',
  function (
    $scope,
    recipesService) {
    $scope.recipes = recipesService.recipes;
}]);
