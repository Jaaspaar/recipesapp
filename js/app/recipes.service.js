 var recipesApp = angular.recipesApp;
recipesApp.factory('recipesService', function() {
  const ingredientsNames = getIngredientsNames();
  const ingredients = getIngredients();
  const mealTypes = getMealTypes();
  const recipes = [
    {
      id: 1,
      title: 'Ryba pod serową pierzynką',
      img: 'imgs/ryba-pod-serowa-pierzynka.png',
      description: 'Ryba w święta to tradycyjna konieczność. Bez względu na to, pod jaką postacią zagości na naszych stołach w święta, po prostu musi stanowić jeden z gwoździ wigilijnego programu. Możesz na przykład zdecydować się na rybę po grecku, zupę rybną czy śledzia w oleju, ale także… rybę pod pierzynką serową zapiekaną w Pomyśle na… Rybę pod serową pierzynką z dodatkiem śmietanki i wody. Całość posyp startym żółtym serem i zapiekaj. Twoi bliscy będą zachwyceni rybą podaną pod serową pierzynką!',
      preparationTime: 10,
      kcal: 600,
      rating: 4.35,
      preparationSteps: [
        'Zawartość torebki dokładnie wymieszaj ze 100ml śmietanki 30% i 100ml zimnej wody. Doprowadź do wrzenia i zdejmij z ognia.',
        'W żaroodpornym naczyniu ułóż 400g filetów z całkowicie rozmrożonej, odsączonej, NIE OSOLONEJ ryby, a następnie zalej przygotowanym gorącym "POMYSŁEM NA..." i posyp startym serem.',
        'Piecz w piekarniku w otwartym naczyniu przez 30 minut(200*C, pieczenie góra-dół, bez termoobiegu). Upieczoną rybę odstaw na 5 minut.'
      ],
      mealType: mealTypes.DINNER,
      isVegetarian: true,
      ingredients: [
        {
          name: ingredientsNames.FISH,
          amount: 400,
          unit: 'g'
        },
        {
          name: ingredientsNames.SOUR_CREAM,
          amount: 100,
          unit: 'ml'
        },
        {
          name: ingredientsNames.HARD_CHEESE,
          amount: 50,
          unit: 'g'
        },
        {
          name: ingredientsNames.WINIARY_POMYSL_NA_RYBE_POD_SEROWA_PIERZYNKA,
          amount: 1,
          unit: 'sz.'
        }
      ]
    }, {
      id: 2,
      title: 'Karp w cytrynowej panierce',
      img: 'imgs/karp-w-cytrynowej-panierce.png',
      description: 'Jeśli chcesz dodać swoim świętom owocową nutkę, przyrządź karpia w cytrynowej panierce. Wystarczy, że dzwonki lub filety karpia obtoczysz w panierce Pomysł na Karpia w cytrynowej panierce WINIARY, a następnie będziesz piec je przez około pół godziny. Twoi bliscy nie będą mogli doczekać się na danie, zwłaszcza że aromaty rozchodzące się po mieszkaniu podczas przyrządzania cytrynowego karpia są zniewalające...',
      preparationTime: 10,
      kcal: 400,
      rating: 4.75,
      preparationSteps: [
        'Filety lub dzwonka z karpia pokrój na kawałki (o grubości max. 2 cm), zmocz rybę wodą i obtocz w panierce.',
        'Tak przygotowaną rybę ułóż na kratce do grillowania i włóż do rozgrzanego do 200*C piekarnika.',
        'Filety piecz przez 20-25 minut, dzwonka przez 25-30 minut.'
      ],
      mealType: mealTypes.DINNER,
      isVegetarian: true,
      ingredients: [
        {
          name: ingredientsNames.CARP,
          amount: 600,
          unit: 'g'
        },
        {
          name: ingredientsNames.WINIARY_POMYSL_NA_KARPIA_W_CYTRYNOWEJ_PANIERCE,
          amount: 1,
          unit: 'sz.'
        }
      ]
    }, {
      id: 3,
      title: 'Deser budyniowy z solonym karmelem i kitkatem',
      img: 'imgs/deser-budyniowy-z-solonym-karmelem-i-kitkatem.png',
      description: 'Deser budyniowy z solonym karmelem i KitKatem to przekąska idealna na Sylwestra. Bursztynowo-złocisty karmel, waniliowo-czekoladowa masa i dodatek batoników KitKat tworzą rozpływającą się w ustach mieszankę smaków, której trudno się oprzeć. Deser budyniowy z karmelem twórz warstwami – pokruszonych batoników, karmelu oraz dwóch smaków budyniu - z waniliowym na wierzchu. Każdy deser ozdób dodatkowo jednym całym batonikiem.',
      preparationTime: 10,
      kcal: 400,
      rating: 4.75,
      preparationSteps: [
        'Do rondla wlej wodę i wsyp cukier. Zagotuj.',
        'Zmniejsz ogień i gotuj 3-4 minuty, aż cukier będzie złocistobrązowy. Nie musisz mieszać – wystarczy od czasu do czasu poruszać garnkiem.',
        'Kiedy cukier będzie już bursztynowo-złocisty, dodaj masło, sól i śmietankę, energicznie mieszając.',
        'Budynie przygotuj zgodnie z instrukcją na opakowaniu i przestudź.',
        'Odłóż tyle całych kawałków batonika, ile porcji deseru chcesz zrobić, a resztę pokrój.',
        'Na dno pucharków wsyp część pokrojonych batoników, polej karmelem i nałóż warstwę budyniu czekoladowego. Następnie znów warstwa pokrojonych batoników, karmel i budyń waniliowy.',
        'Wierzch polej jeszcze karmelem, a każdy pucharek udekoruj całym batonikiem.'
      ],
      mealType: mealTypes.DESSERT,
      isVegetarian: true,
      ingredients: [
        {
          name: ingredientsNames.CHOCOLATE_PUDDING,
          amount: 1,
          unit: 'szt.'
        },
        {
          name: ingredientsNames.VANILLA_PUDDING,
          amount: 1,
          unit: 'sz.'
        },
        {
          name: ingredientsNames.MILK,
          amount: 1,
          unit: 'l'
        },
        {
          name: ingredientsNames.KIT_KAT,
          amount: 4,
          unit: 'sz.'
        },
        {
          name: ingredientsNames.SUGAR,
          amount: 250,
          unit: 'g'
        },
        {
          name: ingredientsNames.WATER,
          amount: 250,
          unit: 'ml'
        },
        {
          name: ingredientsNames.SOUR_CREAM,
          amount: 120,
          unit: 'ml'
        },
        {
          name: ingredientsNames.BUTTER,
          amount: 25,
          unit: 'g'
        },
        {
          name: ingredientsNames.SALT,
          amount: 0.25,
          unit: 'lyżeczki'
        }
      ]
    }];

  function getIngredientsNames() {
    return {
      FISH: 'Ryba (filet)',
      SOUR_CREAM: 'Śmietanka 30%',
      HARD_CHEESE: 'Ser żółty',
      WINIARY_POMYSL_NA_RYBE_POD_SEROWA_PIERZYNKA: 'WINIARY POMYSŁ NA... Rybę pod serową pierzynką',
      CARP: 'Karp (filet lub dzwonki)',
      WINIARY_POMYSL_NA_KARPIA_W_CYTRYNOWEJ_PANIERCE: 'WINIARY POMYSŁ NA... Karpia w cytrynowej panierce',
      SUGAR: 'Cukier',
      WATER: 'Woda',
      BUTTER: 'Masło',
      SALT: 'Sól morska',
      MILK: 'Mleko',
      KIT_KAT: 'KitKat',
      CHOCOLATE_PUDDING: 'Budyń czekoladowy',
      VANILLA_PUDDING: 'Budyń waniliowy'
    };
  }

  function getIngredients() {
    const ingredientsLocal = [
      {
        name: ingredientsNames.FISH,
        img: 'imgs/ingredients/fish.jpg'
      },
      {
        name: ingredientsNames.SOUR_CREAM,
        img: 'imgs/ingredients/sour-cream.jpg'
      },
      {
        name: ingredientsNames.HARD_CHEESE,
        img: 'imgs/ingredients/hard-cheese.jpg'
      },
      {
        name: ingredientsNames.WINIARY_POMYSL_NA_KARPIA_W_CYTRYNOWEJ_PANIERCE,
        img: 'imgs/ingredients/winiary-pomysl-na-karpia-w-cytrynowej-panierce.jpg'
      },
      {
        name: ingredientsNames.WINIARY_POMYSL_NA_RYBE_POD_SEROWA_PIERZYNKA,
        img: 'imgs/ingredients/winiary-pomysl-na-rybe-pod-cytrynowa-piezynka.jpg'
      },
      {
        name: ingredientsNames.CARP,
        img: 'imgs/ingredients/carp.jpg'
      },
      {
        name: ingredientsNames.SUGAR,
        img: 'imgs/ingredients/sugar.jpg'
      },
      {
        name: ingredientsNames.WATER,
        img: 'imgs/ingredients/water.jpg'
      },
      {
        name: ingredientsNames.BUTTER,
        img: 'imgs/ingredients/butter.jpg'
      },
      {
        name: ingredientsNames.SALT,
        img: 'imgs/ingredients/salt.jpg'
      },
      {
        name: ingredientsNames.MILK,
        img: 'imgs/ingredients/milk.jpg'
      },
      {
        name: ingredientsNames.KIT_KAT,
        img: 'imgs/ingredients/kit-kat.png'
      },
      {
        name: ingredientsNames.CHOCOLATE_PUDDING,
        img: 'imgs/ingredients/chocolate-pudding.jpg'
      },
      {
        name: ingredientsNames.VANILLA_PUDDING,
        img: 'imgs/ingredients/vanilla-pudding.jpg'
      }
    ];
    return ingredientsLocal;
  }

  function getMealTypes() {
    return {
      BREAKFAST: 'breakfast',
      DINNER: 'dinner',
      DESSERT: 'dessert',
      SUPPER: 'supper'
    }
  }

  return {
    recipes,
    ingredientsNames,
    ingredients,
    mealTypes
  }
});
